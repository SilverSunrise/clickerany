package fran.jou.clickerany

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var perClick = 1
        var moneyCounter = 0

        val backData = intent
        val backPerClick = backData.getIntExtra("perClickAfterUpdate", perClick)
        val backCurrent = backData.getIntExtra("moneyAfterUpdate", moneyCounter)
        if (backData != null) {
            perClick = backPerClick
            moneyCounter = backCurrent
            tv_counterMoney.text = "Your money: $moneyCounter"
        }


        btn_shop.setOnClickListener {
            val intent = Intent(this, ShopActivity::class.java)
            intent.putExtra("counter", moneyCounter)
            intent.putExtra("perClick", perClick)
            startActivity(intent)
        }

        iv_chest.setOnClickListener {
            moneyCounter += perClick
            tv_counterMoney.text = "Your money: $moneyCounter"
            iv_chest.animate().apply {
                duration = 44
                scaleX(1.09f)
                scaleY(1.09f)
            }.withEndAction {
                iv_chest.animate().apply {
                    duration = 44
                    scaleX(0.99f)
                    scaleY(0.99f)
                }.start()
            }

            when (moneyCounter) {
                in 5..7 -> {
                    moneyCounter += 35
                    tv_counterMoney.text = "Your money: $moneyCounter"
                    chestAnimation()
                }
                in 300..320 -> {
                    moneyCounter += 50
                    tv_counterMoney.text = "Your money: $moneyCounter"
                    chestAnimation()
                }
                in 700..740 -> {
                    moneyCounter += 80
                    tv_counterMoney.text = "Your money: $moneyCounter"
                    chestAnimation()
                }
                in 1200..1300 -> {
                    moneyCounter += 100
                    tv_counterMoney.text = "Your money: $moneyCounter"
                    chestAnimation()
                }

            }
        }
    }

    private fun chestAnimation() {
        iv_chest.animate().apply {
            duration = 90
            iv_chest.setImageResource(R.drawable.chest_open)
        }.withEndAction {
            iv_chest.setImageResource(R.drawable.chest_close)
        }.start()
    }
}