package fran.jou.clickerany

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_shop.*

class ShopActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop)
        var (moneyCounter, perClick) = getData()

        btn_1ShopOption.setOnClickListener {
            if (moneyCounter >= 50) {
                perClick += 2
                moneyCounter -= 50
                showMoney(moneyCounter, perClick)
            }
        }

        btn_2ShopOption.setOnClickListener {
            if (moneyCounter >= 120) {
                perClick += 5
                moneyCounter -= 120
                showMoney(moneyCounter, perClick)
            }
        }

        btn_3ShopOption.setOnClickListener {
            if (moneyCounter >= 300) {
                perClick += 10
                moneyCounter -= 300
                showMoney(moneyCounter, perClick)
            }
        }

        btn_4ShopOption.setOnClickListener {
            if (moneyCounter >= 600) {
                perClick += 22
                moneyCounter -= 600
                showMoney(moneyCounter, perClick)
            }
        }

        btn_5ShopOption.setOnClickListener {
            if (moneyCounter >= 1200) {
                perClick += 40
                moneyCounter -= 1200
                showMoney(moneyCounter, perClick)
            }
        }

        btn_exit.setOnClickListener {
            sendData(moneyCounter, perClick)
        }
    }

    private fun sendData(moneyCounter: Int, perClick: Int) {
        val back = Intent(this, MainActivity::class.java)
        back.putExtra("moneyAfterUpdate", moneyCounter)
        back.putExtra("perClickAfterUpdate", perClick)
        startActivity(back)
    }

    override fun onBackPressed() {

    }

    private fun getData(): Pair<Int, Int> {
        val getIntent = intent
        val moneyCounter = getIntent.getIntExtra("counter", 0)
        val perClick = getIntent.getIntExtra("perClick", 0)
        showMoney(moneyCounter, perClick)
        return Pair(moneyCounter, perClick)
    }

    private fun showMoney(moneyCounter: Int, perClick: Int) {
        tv_currentMoney.text = "Your money: $moneyCounter"
        tv_currentPerClick.text = "Per click: $perClick"
    }
}